package com.gregorywizard.opengeeklabtestapp;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.client.Firebase;

/**
 * Created by gregory on 5/10/16.
 */
public class EmailUsersActivity extends AppCompatActivity {
    Firebase ref;
    ListView listView;
    FirebaseListAdapter<User> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.email_users_activity);

        setTitle("Email Users");
        Firebase.setAndroidContext(this);

        ref = new Firebase(getString(R.string.firebase_id) + "/users");

        listView = (ListView)findViewById(R.id.list);


        adapter = new FirebaseListAdapter<User>(ref, User.class, R.layout.user_layout, this) {

            @Override
            protected void populateView(android.view.View v, User model) {
                ((TextView)v.findViewById(R.id.usertvUsername)).setText(model.getUsername());
                ((TextView)v.findViewById(R.id.usertvEmail)).setText(model.getEmail());
                new DownloadImageTask((ImageView) v.findViewById(R.id.userivPhoto)).execute(model.getPhoto());
            }
        };
        listView.setAdapter(adapter);

    }

    private class DownloadImageTask extends AsyncTask<String, Void, byte[]> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected byte[] doInBackground(String... urls) {
            byte[] decodedBytes = Base64.decode(urls[0], Base64.DEFAULT);
            return decodedBytes;
        }

        protected void onPostExecute(byte[] result) {
            Glide.with(EmailUsersActivity.this).load(result).crossFade().fitCenter().into(bmImage);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(EmailUsersActivity.this, RegEmailActivity.class);
        startActivity(intent);
    }
}
