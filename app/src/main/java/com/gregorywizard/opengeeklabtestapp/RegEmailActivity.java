package com.gregorywizard.opengeeklabtestapp;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by gregory on 5/10/16.
 */
public class RegEmailActivity extends AppCompatActivity {
    Firebase ref;

    EditText etUsername;
    EditText etEmail;
    EditText etPassword;
    EditText etPasswordConf;
    Button   bEmailSignUp;
    Button   bGetPhoto;

    int      REQUEST_CAMERA = 0, SELECT_FILE = 1;
    String   base64Image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reg_email_activity);

        etUsername     = (EditText) findViewById(R.id.etUsername);
        etEmail        = (EditText) findViewById(R.id.etEmail);
        etPassword     = (EditText) findViewById(R.id.etPassword);
        etPasswordConf = (EditText) findViewById(R.id.etPasswordConf);
        bEmailSignUp   = (Button)   findViewById(R.id.bEmailSignUp);
        bGetPhoto      = (Button)   findViewById(R.id.bGetPhoto);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        Firebase.setAndroidContext(this);

        ref = new Firebase(getString(R.string.firebase_id));

        bEmailSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username = etUsername.getText().toString();
                final String email    = etEmail.getText().toString();
                final String pass     = etPassword.getText().toString();
                final String passconf = etPasswordConf.getText().toString();


                if(!pass.equals("") & pass.equals(passconf)) {
                    ref.createUser(email, pass, new Firebase.ResultHandler() {
                        @Override
                        public void onSuccess() {
                            Toast.makeText(getApplicationContext(),"User created.", Toast.LENGTH_LONG).show();
                            //ref.child("users").setValue("Do you have data? You'll love Firebase.");
                            ref.authWithPassword(email, pass,
                                    new Firebase.AuthResultHandler() {
                                        @Override
                                        public void onAuthenticated(AuthData authData) {
                                            // Authentication just completed successfully :)
                                            if(base64Image == null) {
                                                base64Image = "";
                                            }
                                            //Map<String, String> map = new HashMap<String, String>();
                                            //map.put("username", username);
                                            //map.put("email",    email);
                                            //map.put("photo",    base64Image);
                                            //ref.child("users").child(authData.getUid()).setValue(map);
                                            User user = new User(username,email,base64Image);
                                            ref.child("users").child(authData.getUid()).setValue(user);
                                        }
                                        @Override
                                        public void onAuthenticationError(FirebaseError error) {
                                            // Something went wrong :(
                                        }
                                    });
                        }

                        @Override
                        public void onError(FirebaseError firebaseError) {
                            Toast.makeText(getApplicationContext(),"Something wrong.",Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    Toast.makeText(getApplicationContext(),"Passwords are not the same.",Toast.LENGTH_LONG).show();
                }


            }
        });

        bGetPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = { "Take Photo", "Choose from Library",
                        "Cancel" };

                AlertDialog.Builder builder = new AlertDialog.Builder(RegEmailActivity.this);
                builder.setTitle("Add Photo!");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Take Photo")) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, REQUEST_CAMERA);
                        } else if (items[item].equals("Choose from Library")) {
                            Intent intent = new Intent(
                                    Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("image/*");
                            startActivityForResult(
                                    Intent.createChooser(intent, "Select File"),
                                    SELECT_FILE);
                        } else if (items[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        InputStream inputStream = null;//You can get an inputStream using any IO API
        try {
            inputStream = new FileInputStream(destination.getAbsolutePath());
        } catch(FileNotFoundException e) {

        }
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);
        try {
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output64.write(buffer, 0, bytesRead);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            output64.close();
        }catch(IOException e) {}
        base64Image = output.toString();

        Toast.makeText(this,"Capture Image is successfully loaded.",Toast.LENGTH_LONG).show();
    }

    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);

        Bitmap bm2;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        bm2 = BitmapFactory.decodeFile(selectedImagePath, options);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm2.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] bytes = baos.toByteArray();
        base64Image = Base64.encodeToString(bytes, Base64.DEFAULT);

        Toast.makeText(this,"Image is successfully loaded.",Toast.LENGTH_LONG).show();

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RegEmailActivity.this, MainActivity.class);
        startActivity(intent);
    }
}