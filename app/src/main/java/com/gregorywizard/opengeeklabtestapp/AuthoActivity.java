package com.gregorywizard.opengeeklabtestapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by gregory on 5/10/16.
 */
public class AuthoActivity extends AppCompatActivity {
    Firebase      ref;

    EditText      etEmail;
    EditText      etPassword;
    Button        bSignIn;
    LoginButton   bFacebook;

    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.autho_activity);
        setTitle("Authorization");

        //AppEventsLogger.activateApp(this);
        Firebase.setAndroidContext(this);
        callbackManager = CallbackManager.Factory.create();

        etEmail    = (EditText)   findViewById(R.id.aetEmail);
        etPassword = (EditText)   findViewById(R.id.aetPassword);
        bSignIn    = (Button)     findViewById(R.id.abSignIn);
        bFacebook  = (LoginButton)findViewById(R.id.abFacebook);
        //bFacebook.setReadPermissions("email");
        //bFacebook.setReadPermissions("user_posts");
        bFacebook.setReadPermissions("user_posts");

        ref = new Firebase(getString(R.string.firebase_id));

        bSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = etEmail.getText().toString();
                String pass  = etPassword.getText().toString();
                if(!email.equals("") && !pass.equals("")) {
                    ref.authWithPassword(email, pass, new Firebase.AuthResultHandler() {
                        @Override
                        public void onAuthenticated(AuthData authData) {
                            Toast.makeText(getApplicationContext(),"User ID: " + authData.getUid(),Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(AuthoActivity.this, EmailUsersActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_right);
                        }

                        @Override
                        public void onAuthenticationError(FirebaseError firebaseError) {
                            // there was an error
                            Toast.makeText(getApplicationContext(),firebaseError.toString(),Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    Toast.makeText(getApplicationContext(),"Input email and password.",Toast.LENGTH_LONG).show();
                }
            }
        });

        // Callback registration
        bFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d("onSuccess",loginResult.toString());
                GraphRequestAsyncTask graphRequestAsyncTask = new GraphRequest(
                        loginResult.getAccessToken(),
                        "/me/posts",
                        null,
                        HttpMethod.GET,
                        new GraphRequest.Callback() {
                            public void onCompleted(GraphResponse response) {
                                Intent intent = new Intent(AuthoActivity.this,FacebookPostsActivity.class);
                                try {
                                    JSONArray rawName = response.getJSONObject().getJSONArray("data");
                                    System.out.println(response.toString());
                                    intent.putExtra("jsondata", rawName.toString());
                                    startActivity(intent);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                Log.d("onCancel","");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d("FacebookException",exception.getMessage());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AuthoActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
