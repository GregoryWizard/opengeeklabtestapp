package com.gregorywizard.opengeeklabtestapp;

/**
 * Created by gregory on 5/10/16.
 */
class User
{
    private String username;
    private String email;
    private String photo;

    public User() {}

    public User(String username, String email, String photo) {
        this.username = username;
        this.email = email;
        this.photo = photo;
    }

    public String getUsername()
    {
        return username;
    }

    public String getEmail()
    {
        return email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}

